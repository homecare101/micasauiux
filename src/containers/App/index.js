import React from 'react';
import Home from '../Home'
import About from '../About'
import User from '../User'
import { Route, Switch,BrowserRouter } from 'react-router-dom'
import { connect } from 'react-redux';

class App extends React.Component {

  render() {
    return (
      <div>
        <BrowserRouter>
		  	<Switch>
		      <Route exact path="/home" render={props=><Home {...props} />} />
		      <Route exact path="/about" render={props=><About {...props} />} />
		      <Route exact path="/user" render={props=><User {...props} />} />
		    </Switch> 
		</BrowserRouter>
      </div>
    );
  }
}

//const mapStateToProps = state => ({});

export default connect(state => (mapDispatch))(App);

const mapDispatch = dispatch => {
  const allActionProps = Object.assign({}, dispatch);
  return allActionProps;
};
