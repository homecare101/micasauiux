import React from 'react';
import { connect } from 'react-redux';
import { Query, Mutation } from "react-apollo";
import { GET_USER_LIST, ADD_USER } from "../../actions/queryStrings"
import NavBar from '../../components/Navigations/NavBar'
import {Grid, Row, Col, Table, Modal, Button, FormGroup, FormControl} from 'react-bootstrap'


class User extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      userModal: false,
      userData: {
        name: '',
        email: ''
      }
    }
  }

  handleClose() {
    this.setState({ userModal: false });
  }

  handleOpen() {
    this.setState({ userModal: true });
  }

  onChange(key, event){
    let {userData} = this.state;
    userData[key] = event.target.value;
    this.setState({userData})
  }

  onAddUser(mutation, event){
    event.preventDefault();
    let {userData} = this.state;
    mutation({ variables: { name: userData.name, email: userData.email } });

    this.handleClose();
    userData = {name: '', email: ''}
    this.setState({userData})
  }


  render() {
    let {userData} = this.state;
    return (
      <div>
        <NavBar />
        <Grid>
          <Row>
            <Col md={12} className="text-right" style={{marginBottom: 20}}>
              <Button onClick={this.handleOpen.bind(this)}>Add User</Button>
            </Col>
            <Col md={12}>
              <Table striped bordered condensed hover>
                <thead>
                  <tr>
                    <th>User Name</th>
                    <th>Email</th>
                  </tr>
                </thead>
                <Query query={GET_USER_LIST}>
                  {({ loading, error, data }) => {
                    if (loading) return <tbody><tr><td className="text-center" colSpan="2">Loading...</td></tr></tbody>;
                    if (error) return <p>Error :(</p>;
                    if (data && data.users && data.users.length) {
                      return <tbody>
                      {data.users.map(({ name, email, _id }) => {
                        return <tr key={_id}>
                          <td>
                            {`${name}`}
                          </td>
                          <td> 
                            {`${email}`}
                          </td>
                        </tr>
                      })}
                      </tbody>
                    }
                    else{
                      return <tbody>
                        <tr>
                          <td colSpan="2" className="text-center">No Data to show</td>
                        </tr>
                      </tbody>
                    }
                  }}
                </Query>
              </Table>
            </Col>
          </Row>
        </Grid>


        <Modal show={this.state.userModal} onHide={this.handleClose.bind(this)}>
          <Modal.Header closeButton>
            <Modal.Title>Add User</Modal.Title>
          </Modal.Header>
          <Modal.Body>
            <Mutation 
              mutation={ADD_USER}
              update={(cache, { data: { createUser } }) => {
                const { users } = cache.readQuery({ query: GET_USER_LIST });
                cache.writeQuery({
                  query: GET_USER_LIST,
                  data: { users: users.concat([createUser]) }
                });
              }}
            >
              {(createUser, { data }) => (
                <form
                  onSubmit={this.onAddUser.bind(this, createUser)}
                >
                  <FormGroup>
                    <FormControl
                      type="text"
                      placeholder="Enter Name"
                      value={userData.name}
                      onChange={this.onChange.bind(this, 'name')}
                    />
                  </FormGroup>

                  <FormGroup>
                    <FormControl
                      type="email"
                      placeholder="Enter Email"
                      value={userData.email}
                      onChange={this.onChange.bind(this, 'email')}
                    />
                  </FormGroup>
                  
                  <Button type="submit">Submit</Button>
                </form>
              )}
            </Mutation>

          </Modal.Body>
        </Modal>
      </div>
    );
  }
}

//const mapStateToProps = state => ({});

export default connect(state => (mapDispatch))(User);

const mapDispatch = dispatch => {
  const allActionProps = Object.assign({}, dispatch);
  return allActionProps;
};
