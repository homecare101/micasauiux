import React from 'react';
import { connect } from 'react-redux';
import NavBar from '../../components/Navigations/NavBar'
import {Grid, Row, Col} from 'react-bootstrap'


class Home extends React.Component {

  render() {
    return (
      <div>
        <NavBar />
        <Grid>
          <Row>
            <Col md={12}>
              <h2 className="text-center">Home</h2>
            </Col>
          </Row>
        </Grid>
      </div>
    );
  }
}

//const mapStateToProps = state => ({});

export default connect(state => (mapDispatch))(Home);

const mapDispatch = dispatch => {
  const allActionProps = Object.assign({}, dispatch);
  return allActionProps;
};
