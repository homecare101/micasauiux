import gql from "graphql-tag";

export const GET_USER_LIST = gql`{users{_id name email}}`

export const ADD_USER = gql`
  mutation createUser($name: String!, $email: String!) {
    createUser(input:{name: $name, email: $email}) {
      _id
      name
      email
    }
  }
`;