import React from 'react';
import { ApolloClient } from 'apollo-client';
import { withClientState } from 'apollo-link-state';
import { InMemoryCache } from 'apollo-cache-inmemory';
import { HttpLink } from 'apollo-link-http';
import { ApolloLink } from 'apollo-link';
import { defaults, resolvers } from './resolvers/users';

const cache = new InMemoryCache();
const stateLink = withClientState({ resolvers, cache, defaults })
const client = new ApolloClient({
  cache,
  link: ApolloLink.from([stateLink, new HttpLink({
    uri: `http://209.97.142.219:3030/graphql`,
  })]),
});

export default client;