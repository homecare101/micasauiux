import React from 'react';
import { connect } from 'react-redux';
import { NavLink } from 'react-router-dom';
import {Navbar, Nav} from 'react-bootstrap'

class NavBar extends React.Component {

  render() {

    return (
      <div>
        <Navbar>
          <Navbar.Header>
            <Navbar.Brand>
              <a href="#home">Micasacare</a>
            </Navbar.Brand>
          </Navbar.Header>
          <Nav>
            <li>
              <NavLink to={'/home'}>
                Home
              </NavLink>
            </li>
            <li>
              <NavLink to={'/about'}>
                About
              </NavLink>
            </li>
            <li>
              <NavLink to={'/user'}>
                Users
              </NavLink>
            </li>
          </Nav>
        </Navbar>
      </div>
    );
  }
}

export default connect(state => (mapDispatch))(NavBar);

const mapDispatch = dispatch => {
  const allActionProps = Object.assign({}, dispatch);
  return allActionProps;
};
