import React from 'react';
import { render } from 'react-dom';
import { ApolloProvider } from 'react-apollo';
import { Provider } from 'react-redux'
import store from './store'
import App from './containers/App';
import './styles/custom.css'
import client from './client'

const target = document.querySelector('#root')

render(
    <Provider store={store}>
      <ApolloProvider client={client}>
            <div>
              <App />
            </div>
      </ApolloProvider>
    </Provider>,
  target
)